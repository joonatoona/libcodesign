#include "elfparse.h"

struct ElfFile open_elf(const char* filename) {
    struct ElfFile ef;

    ef.fd = fopen(filename, "r");
    fseek(ef.fd, 0, SEEK_SET);
    fread(&ef.header, 1, sizeof(Elf64_Ehdr), ef.fd);

    ef.table = malloc(ef.header.e_shentsize * ef.header.e_shnum);
    fseek(ef.fd, ef.header.e_shoff, SEEK_SET);
    fread(ef.table, 1, ef.header.e_shentsize * ef.header.e_shnum, ef.fd);

    return ef;
}

void destroy_elffile(struct ElfFile* f) {
    fclose(f->fd);
    free(f->table);
}

char* get_data(struct ElfFile* elf, const char* sectionName, int* size) {
    int i;
    char* sh_str;
    char* buff;

    buff = malloc(elf->table[elf->header.e_shstrndx].sh_size);

    if (buff != NULL) {
        fseek(elf->fd, elf->table[elf->header.e_shstrndx].sh_offset, SEEK_SET);
        fread(buff, 1, elf->table[elf->header.e_shstrndx].sh_size, elf->fd);
    }
    sh_str = buff;

    for (i = 0; i < elf->header.e_shnum; i++) {
        if (!strcmp(sectionName, (sh_str + elf->table[i].sh_name))) {
            break;
        }
    }

    free(buff);

    /*Code to print or store string data*/
    if (i < elf->header.e_shnum) {
        (*size) = elf->table[i].sh_size;
        char* data = malloc((*size));
        fseek(elf->fd, elf->table[i].sh_offset, SEEK_SET);
        fread(data, 1, (*size), elf->fd);
        return data;
    } else {
        (*size) = -1;
        return NULL;
    }
}
