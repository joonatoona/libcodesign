#ifndef __ELFPARSE_H
#define __ELFPARSE_H

#include <elf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ElfFile {
    FILE* fd;
    Elf64_Ehdr header;
    Elf64_Shdr* table;
};

struct ElfFile open_elf(const char* filename);

void destroy_elffile(struct ElfFile* f);

char* get_data(struct ElfFile* elf, const char* sectionName, int* size);

#ifdef __cplusplus
}
#endif

#endif
