#include "elfparse.h"

int main() {
    struct ElfFile f = open_elf("../../test");
    int size;
    char* comment = get_data(&f, ".comment", &size);
    if (comment == NULL) {
        printf("Section not found!\n");
    } else {
        printf("%s (%d)\n", comment, size);
        free(comment);
    }
    destroy_elffile(&f);
    return 0;
}
